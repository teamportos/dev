rangeOccupied = []

def readAddress(line):
  if '0x' not in line:
    return (0, 0)

  address = ''
  start = line.find('0x')
  addlen = 18

  if len(line) < start+addlen:
    return (0, 0)

  address = line[start:start+18]

  if ' ' in address:
    return (0, 0)

  line = line[start+addlen:]

  if '0x' not in line:
    return (0, 0)

  start = line.find('0x')
  data = ''
  i = 0
  while start+i < len(line) and line[start+i] != ' ':
    data += line[start+i]
    i += 1

  if len(data) >= 10:
    return (0, 0)

  try:
    address = int(address, base=16)
    data = int(data, base=16)
  except ValueError:
    return (0, 0)

  if address == 0 or data == 0:
    return (0, 0)

  if address not in rangeOccupied:
    rangeOccupied.extend(range(address, address+data))
  else:
    return (0, 0)

  return (address, data)

def groupMemory(address):
  memory = [
    {
      "name": "Ram 1",
      "range": ("0x10000000", "0x10008000")
    },
    {
      "name": "Ram 2",
      "range": ("0x10080000", "0x1008A000"),
    },
    {
      "name": "Flash 1",
      "range": ("0x1A000000", "0x1A080000"),
    },
    {
      "name": "Flash 2",
      "range": ("0x1B000000", "0x1B080000"),
    }
  ]
  for m in memory:
    start, end = m["range"]
    start = int(start, base=16)
    end = int(end, base=16)
    if address >= start and address < end:
      return m["name"] 
  return ''

def main(filepath):
  used_mem = {}
  with open(filepath, 'r') as mapfile:
    lines = mapfile.readlines()
    for l in lines:
      address, data = readAddress(l)
      if address:
        group = groupMemory(address)
        if group:
          if group not in used_mem:
            used_mem[group] = 0
          used_mem[group] += data
  print(used_mem)

if __name__ == '__main__':
  import sys
  if len(sys.argv) == 1:
    print('Usage: inspect /path/to/map')
  else:
    main(sys.argv[1])