## Proyecto Eventos OSEK
### Versión estática
Flash 1   11764 Bytes
Ram 1     1716  Bytes
Stack     1536  Bytes
Uso RAM   180   Bytes

### Versión dinámica
Flash 1   11668 Bytes
Ram 1     1812  Bytes
Stack     1536  Bytes
Uso RAM   276   Bytes

### Diferencias
Flash 1   96    Bytes
Ram 1     96    Bytes
Stack     0     Bytes
Uso RAM   96    Bytes

Porcentaje Incremento
    Uso RAM   53.3%

## Proyecto Simple OSEK
### Versión estática
Flash 1   8324  Bytes
Ram 1     1136  Bytes
Stack     1024  Bytes
Uso RAM   112   Bytes

### Versión dinámica
Flash 1   8144  Bytes
Ram 1     1204  Bytes
Stack     1024  Bytes
Uso RAM   180   Bytes

### Diferencias
Flash 1   180   Bytes
Ram 1     68    Bytes
Stack     0     Bytes
Uso RAM   68    Bytes

Porcentaje Incremento
    Uso RAM   60.7%

## Proyecto Simple OSEK
### Versión estática
Flash 1   8720  Bytes
Ram 1     5968  Bytes
Stack     5632  Bytes
Uso RAM   336   Bytes

### Versión dinámica
Flash 1   8144  Bytes
Ram 1     6364  Bytes
Stack     5632  Bytes
Uso RAM   732   Bytes

### Diferencias
Flash 1   576   Bytes
Ram 1     396   Bytes
Stack     0     Bytes
Uso RAM   396   Bytes

Porcentaje Incremento
    Uso RAM   117.9%

Regresion	y = 6.418681*x + 53.764831