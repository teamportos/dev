#include "Os_Internal.h"

void * GetDynamicParam()
{
   void * DynParam = NULL;

   IntSecure_Start();

   if (GetRunningTask() >= TASKS_COUNT)
   {
      DynParam = DynTaskParams[GetRunningTask() - TASKS_COUNT];
   }

   IntSecure_End();

   return DynParam;
}