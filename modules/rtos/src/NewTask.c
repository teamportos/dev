#include "Os_Internal.h"

TaskType NewTask (
   void (*fun)(void),
   void *params
) 
{
   TaskType taskid = INVALID_TASK;
   IntSecure_Start();
   if ( GetDynamicTaskCount() < DYNAMIC_MAX )
   {
      uint8 found = 0;

      for (taskid = TASKS_COUNT + GetDynamicTaskCount(); taskid < TASKS_COUNT + DYNAMIC_MAX; ++taskid)
      {
         if (TasksConst[taskid].EntryPoint == NULL)
         {
            found = 1;
            break;
         }
      }

      if (!found)
      {
         for (taskid = TASKS_COUNT; taskid < TASKS_COUNT + GetDynamicTaskCount() && TasksConst[taskid].EntryPoint != NULL; ++taskid);
      }

      TasksConst[taskid].EntryPoint = fun;
      #ifdef DYNAMIC_PRIORITY
      TasksConst[taskid].StaticPriority = DYNAMIC_PRIORITY;
      #endif
      TasksVar[taskid].Flags.State = TASK_ST_SUSPENDED;
      DynTaskParams[taskid - TASKS_COUNT] = params;

      #if (ARCH == cortexM4 || ARCH == cortexM0)
      InitStack_Arch(taskid);
      #endif

      IncrementDynamicTaskCount();

      SetEntryPoint(taskid);
   }
   IntSecure_End();
   return taskid;
}