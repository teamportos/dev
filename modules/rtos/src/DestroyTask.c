#include "Os_Internal.h"

StatusType DestroyTask()
{
    TaskType taskid = GetRunningTask();
    StatusType ret = E_OK;

    IntSecure_Start();

    ReleaseInternalResources();

    switch(TasksVar[taskid].Flags.State) {

        case TASK_ST_READY:
            RemoveTask(taskid);
            TasksVar[taskid].Activations = 0;

        case TASK_ST_SUSPENDED:
            TasksVar[taskid].Flags.State = TASK_ST_INVALID;
            TasksConst[taskid].EntryPoint = NULL;
            break;

        default:
            break;
    }

    DecrementDynamicTaskCount();

    IntSecure_End();
    
    return ret;
}