#include "Os_Internal.h"

StatusType DecreasePriority(
    TaskType taskid,
    uint8 num
)
{
    StatusType ret = E_OK;

#if (ERROR_CHECKING_TYPE == ERROR_CHECKING_EXTENDED)
/*
    if ((TasksVar[taskid].ActualPriority - num) < 0)
    {
        ret = E_OS_ACCESS;
    }
*/
    #if (RESOURCES_COUNT != 0)
    if( TasksVar[taskid].Resources != 0 )
    {
        ret = E_OS_RESOURCE;
    }
    #endif
#endif

#if (HOOK_POSTTASKHOOK == OSEK_ENABLE)
      PostTaskHook();
#endif /* #if (HOOK_POSTTASKHOOK == OSEK_ENABLE) */

    IntSecure_Start();
/*
    if (ret != E_OK)
    {
        SetError_Ret(ret);
        SetError_Msg("DecreasePriority returns != E_OK");
        SetError_ErrorHook();
        return ret;
    }
*/
    TasksConst[taskid].StaticPriority -= num;
    TasksVar[taskid].ActualPriority = TasksConst[taskid].StaticPriority;

    IntSecure_End();

#if (HOOK_ERRORHOOK == OSEK_ENABLE)
   /* \req OSEK_ERR_1.3-2/xx The ErrorHook hook routine shall be called if a
    ** system service returns a StatusType value not equal to E_OK.*/
   /* \req OSEK_ERR_1.3.1-2/xx The hook routine ErrorHook is not called if a
    ** system service is called from the ErrorHook itself. */
   if ( ( ret != E_OK ) && (ErrorHookRunning != 1))
   {
      SetError_Api(DecreasePriority);
      SetError_Ret(ret);
      SetError_Msg("Terminate Task returns != than E_OK");
      SetError_ErrorHook();
   }
#endif

    return ret;
}