/********************************************************
 * DO NOT CHANGE THIS FILE, IT IS GENERATED AUTOMATICALY*
 ********************************************************/

/* Copyright 2008, 2009, 2014, 2015 Mariano Cerdeiro
 * Copyright 2014, ACSE & CADIEEL
 *      ACSE: http://www.sase.com.ar/asociacion-civil-sistemas-embebidos/ciaa/
 *      CADIEEL: http://www.cadieel.org.ar
 *
 * This file is part of CIAA Firmware.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** \brief FreeOSEK Os Generated Internal Configuration Implementation File
 **
 ** \file Os_Internal_Cfg.c
 **
 **/

/** \addtogroup FreeOSEK
 ** @{ */
/** \addtogroup FreeOSEK_Os
 ** @{ */
/** \addtogroup FreeOSEK_Os_Internal
 ** @{ */

/*==================[inclusions]=============================================*/
#include "Os_Internal.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/
unsigned int DynamicTaskCount = 0;

<?php

$this->loadHelper("modules/rtos/gen/ginc/Multicore.php");

/* get tasks */
$tasks = $this->helper->multicore->getLocalList("/OSEK", "TASK");

foreach ($tasks as $task)
{
   print "/** \brief $task stack */\n";
   print "#if ( x86 == ARCH )\n";
   print "uint8 StackTask" . $task . "[" . $this->config->getValue("/OSEK/" . $task, "STACK") ." + TASK_STACK_ADDITIONAL_SIZE];\n";
   print "#else\n";
   print "uint8 StackTask" . $task . "[" . $this->config->getValue("/OSEK/" . $task, "STACK") ."];\n";
   print "#endif\n";
}
print "\n";

foreach ($tasks as $task)
{
   print "/** \brief $task context */\n";
   print "TaskContextType ContextTask" . $task . ";\n";
}
print "\n";

$priority = $this->config->priority2osekPriority($tasks);

$dyn = $this->config->getList("/OSEK","DYNAMIC");
$dynalarms = [];
$dyncount = 0;
$dynresources = [];
$dynevents = [];
if (count($dyn) > 0) 
{
   $dynprio = $this->config->getValue("/OSEK/".$dyn[0], "PRIORITY") - 1;
   $dyncount = $this->config->getValue("/OSEK/".$dyn[0], "MAX");
   $stacksize = $this->config->getValue("/OSEK/".$dyn[0], "STACK");
   $dynresources = $this->config->getList("/OSEK/".$dyn[0], "RESOURCE");
   $dynevents = $this->config->getList("/OSEK/".$dyn[0], "EVENT");
}

if(count($dyn) > 0 && $stacksize < 1) 
{
   throw new Exception("Undefined Stack Size for Dynamic Tasks");
}

/* Ready List */
$found_prio = 0;
$dyntotal = 0;

foreach ($priority as $prio)
{
   print "/** \brief Ready List for Priority $prio */\n";
   $count = 0;
   foreach ($tasks as $task)
   {
      if ($priority[$this->config->getValue("/OSEK/" . $task, "PRIORITY")] == $prio)
      {
         $count += $this->config->getValue("/OSEK/" . $task, "ACTIVATION");
      }
   }
   if(count($dyn) > 0)
   {
      $dyntotal = $this->config->getValue("/OSEK/".$dyn[0], "ACTIVATION") * $dyncount;
      if ($dynprio == $prio)
      {
         $count += $dyntotal;
         $found_prio = 1;
      }
   }
   print "TaskType ReadyList" . $prio . "[" . $count . "];\n\n";
}
if ($found_prio == 0 && count($dyn) > 0)
{
   print "TaskType ReadyList" . $dynprio . "[" . $dyntotal . "];\n\n";
}

// Dynamic tasks structures
if (count($dyn) > 0) 
{
  for ($i = 0; $i < $dyncount; ++$i) 
  {
    print "/** \brief dynamic $i stack */\n";
    print "#if ( x86 == ARCH )\n";
    print "uint8 StackDynTask" . $i . "[" . $stacksize . " + TASK_STACK_ADDITIONAL_SIZE];\n";
    print "#else\n";
    print "uint8 StackDynTask" . $i . "[" . $stacksize . "];\n";
    print "#endif\n";
    print "TaskContextType ContextDynTask" . $i . ";\n";
  }
}

$counters = $this->helper->multicore->getLocalList("/OSEK", "COUNTER");
$alarms = $this->helper->multicore->getLocalList("/OSEK", "ALARM");

foreach ($counters as $counter)
{
   $countalarms = 0;
   foreach ($alarms as $alarm)
   {
      if ($this->config->getValue("/OSEK/" . $alarm . "/SETEVENT","TASK") == "DYNAMIC" || 
         $this->config->getValue("/OSEK/" . $alarm . "/ACTIVATETASK","TASK") == "DYNAMIC")
      {
         if (!in_array($alarm, $dynalarms))
         {
            $dynalarms[] = $alarm;
         }
         $countalarms += $dyncount;
      }
      else if ($counter == $this->config->getValue("/OSEK/" . $alarm,"COUNTER"))
      {
         $countalarms++;
      }
   }
   print "const AlarmType OSEK_ALARMLIST_" . $counter . "[" . $countalarms . "] = {\n";
   foreach ($alarms as $alarm)
   {
      if ($dyncount > 0 && in_array($alarm, $dynalarms) && $counter == $this->config->getValue("/OSEK/" . $alarm,"COUNTER"))
      {
        for ($i = 0; $i < $dyncount; $i++)
        {
         print "   $alarm" . " + $i, /* this alarm has to be incremented with this counter */\n";
        }
      }
      else if ($counter == $this->config->getValue("/OSEK/" . $alarm,"COUNTER"))
      {
         print "   $alarm, /* this alarm has to be incremented with this counter */\n";
      }
   }
   print "};\n\n";
}

?>

/*==================[external data definition]===============================*/
/* FreeOSEK to configured priority table
 *
 * This table show the relationship between the user selected
 * priorities and the OpenOSE priorities:
 *
 * User P.         Osek P.
<?php
$pkey = array_keys($priority);
for($i = 0; $i < count($priority); $i++)
{
   print " * " . $pkey[$i] . "               " . $priority[$pkey[$i]] . "\n";
}
print " */\n";

?>
<?php
print "TaskConstType TasksConst[TASKS_COUNT + DYNAMIC_MAX] = {";

/* create task const structure */
foreach ($tasks as $count=>$task)
{
   $pri = $priority[$this->config->getValue("/OSEK/" . $task, "PRIORITY")];
   if ( $count != 0 ) print ",\n";
   print "   /* Task $task */\n";
   print "   {\n";
   print "       OSEK_TASK_$task,   /* task entry point */\n";
   print "       &ContextTask" . $task . ", /* pointer to task context */\n";
   print "       StackTask" . $task . ", /* pointer stack memory */\n";
   print "       sizeof(StackTask" . $task . "), /* stack size */\n";
   print "       " . $pri . ", /* task priority */\n";
   print "       " . $this->config->getValue("/OSEK/" . $task, "ACTIVATION"). ", /* task max activations */\n";
   print "       {\n";
   $extended = $this->config->getValue("/OSEK/" . $task, "TYPE");
   if($extended == "BASIC")
   {
      print "         0, /* basic task */\n";
   }
   elseif ($extended == "EXTENDED")
   {
      print "         1, /* extended task */\n";
   }
   else
   {
     $this->log->error("Wrong definition of task type \"" . $extended . "\" for task \"" . $task . "\".");
   }
   $schedule = $this->config->getValue("/OSEK/" .$task, "SCHEDULE");
   if ($schedule == "FULL")
   {
      print "         1, /* preemtive task */\n";
   }
   elseif($schedule == "NON")
   {
      print "         0, /* non preemtive task */\n";
   }
   else
   {
     $this->log->error("Wrong definition of task schedule \"" . $schedule . "\" for task \"" . $task . "\".");
   }
   print "         0\n";
   print "      }, /* task const flags */\n";
   $events = $this->config->getList("/OSEK/" . $task, "EVENT");
   $elist = "0 ";
   foreach ($events as $event)
   {
      $elist .= "| $event ";
   }
   print "      $elist, /* events mask */\n";
   $rlist = "0 ";
   $resources = $this->config->getList("/OSEK/" . $task, "RESOURCE");
   foreach($resources as $resource)
   {
      $rlist .= "| ( 1 << $resource ) ";
   }
   print "      $rlist,/* resources mask */\n";
   if (isset($this->definitions["MCORE"]))
   {
      print "      " . $this->config->getValue("/OSEK/" . $task, "CORE") . " /* core */\n";
   }
   else
   {
      print "      0 /* core */\n";
   }
   print "   }";
}

if (count($dyn) > 0) 
{
   for ($i = 0; $i < $dyncount; ++$i) 
   {
      if ($i+1 !== $dyncount) 
      {
         print ",";
      }
      print "\n";
      print "   /* Dynamic Task $i */\n";
      print "   {\n";
      print "       NULL,   /* task entry point, initially null */\n";
      print "       &ContextDynTask" . $i . ", /* pointer to task context */\n";
      print "       StackDynTask" . $i . ", /* pointer stack memory */\n";
      print "       sizeof(StackDynTask" . $i . "), /* stack size */\n";
      print "       " . $dynprio . ", /* task priority */\n";
      print "       1, /* task max activations */\n";
      $rdynlist = "0 ";
      foreach($dynresources as $dynresource)
      {
         $rdynlist .= "| ( 1 << $dynresource ) ";
      }
      $edynlist = "0 ";
      foreach ($dynevents as $dynevent)
      {
         $edynlist .= "| $dynevent ";
      }
      print "       {
            1, /* extended task */
            1, /* preemtive task */
            0
         }, /* task const flags */
         $edynlist , /* events mask */
         $rdynlist ,/* resources mask */
         0 /* core */\n";
      print "   }";
   }
}
print "\n";
?>
};

/** \brief Dynmaic Tasks Params Vector */
void * DynTaskParams[DYNAMIC_MAX];

/** \brief RemoteTaskCore Array */
const TaskCoreType RemoteTasksCore[REMOTE_TASKS_COUNT] = {<?php
$rtasks = $this->helper->multicore->getRemoteList("/OSEK", "TASK");

for($i=0; $i<count($rtasks); $i++)
{
   print $this->config->getValue("/OSEK/$rtasks[$i]", "CORE");
   if ($i < (count($rtasks)-1))
   {
      print ", ";
   }
}
?>
};

/** \brief TaskVar Array */
TaskVariableType TasksVar[TASKS_COUNT + DYNAMIC_MAX];

<?php
$appmodes = $this->config->getList("/OSEK", "APPMODE");

foreach ($appmodes as $appmode)
{
   $tasksinmode = array();
   foreach($tasks as $task)
   {
      $taskappmodes = $this->config->getList("/OSEK/" . $task, "APPMODE");
      foreach ($taskappmodes as $taskappmode)
      {
         if ($taskappmode == $appmode)
         {
            $tasksinmode[] = $task;
         }
      }
   }
   if (count($tasksinmode) > 0)
   {
      print "/** \brief List of Auto Start Tasks in Application Mode $appmode */\n";
      print "const TaskType TasksAppMode" . $appmode . "[" . count($tasksinmode). "]  = {\n";
      foreach($tasksinmode as $count=>$task)
      {
         if ($count != 0) print ",\n";
         print "   $task";
      }
      print "\n};\n";
   }
}

print "/** \brief AutoStart Array */\n";
print "const AutoStartType AutoStart[" . count($appmodes) . "]  = {\n";

foreach ($appmodes as $count=>$appmode)
{
   if ( $count != 0 ) print ",\n";
   print "   /* Application Mode $appmode */\n";
   print "   {\n";
   $tasksinmode = array();
   foreach($tasks as $task)
   {
      $taskappmodes = $this->config->getList("/OSEK/" . $task, "APPMODE");
      foreach ($taskappmodes as $taskappmode)
      {
         if ($taskappmode == $appmode)
         {
            $tasksinmode[] = $task;
         }
      }
   }
   print "      " . count($tasksinmode) .", /* Total Auto Start Tasks in this Application Mode */\n";
   if (count($tasksinmode)>0)
   {
      print "      (TaskRefType)TasksAppMode" . $appmode . " /* Pointer to the list of Auto Start Stacks on this Application Mode */\n";
   }
   else
   {
      print "      NULL /* no tasks on this mode */\n";
   }
   print "   }";
}
print "\n};\n";
?>

<?php
print "const ReadyConstType ReadyConst[" . (count($priority) + ($found_prio==0 && count($dyn) > 0 ? 1 : 0)) .  "] = { \n";

if ($found_prio == 0 && count($dyn) > 0) 
{
   print "   {\n";
   print "      $dyntotal, /* Length of this ready list */\n";
   print "      ReadyList$dynprio /* Pointer to the Ready List */\n";
   print "   },\n";
}
$c = 0;
foreach ($priority as $prio)
{
   if ($c++ != 0) print ",\n";
   print "   {\n";
   $count = 0;
   foreach ($tasks as $task)
   {
      if ($priority[$this->config->getValue("/OSEK/" . $task, "PRIORITY")] == $prio)
      {
         $count += $this->config->getValue("/OSEK/" . $task, "ACTIVATION");
      }
   }
   if (count($dyn) > 0 && $dynprio == $prio)
   {
      $count += $dyntotal;
   }
   print "      $count, /* Length of this ready list */\n";
   print "      ReadyList" . $prio . " /* Pointer to the Ready List */\n";
   print "   }";
}
print "\n};\n\n";

print "/** TODO replace next line with: \n";
print " ** ReadyVarType ReadyVar[" . (count($priority) + ($found_prio == 0 && count($dyn) > 0 ? 1 : 0)) . "] ; */\n";
print "ReadyVarType ReadyVar[" . (count($priority) + ($found_prio == 0 && count($dyn) > 0 ? 1 : 0)) . "];\n";
?>

<?php
/* Resources Priorities */
$resources = $this->config->getList("/OSEK","RESOURCE");
print "/** \brief Resources Priorities */\n";
print "const TaskPriorityType ResourcesPriority[" . count($resources) . "]  = {\n";
$c = 0;

foreach ($resources as $resource)
{
   $count = 0;
   foreach ($tasks as $task)
   {
      $resorucestask = $this->config->getList("/OSEK/" . $task, "RESOURCE");
      foreach($resorucestask as $rt)
      {
         if ($rt == $resource)
         {
            if ($priority[$this->config->getValue("/OSEK/" . $task, "PRIORITY")] > $count)
            {
               $count = $priority[$this->config->getValue("/OSEK/" . $task, "PRIORITY")];
            }
         }
      }
   }
   foreach($dynresources as $dynres)
   {
     if($dynres == $resource)
     {
       if($dynprio > $count)
       {
         $count = $dynprio;
       }
     }
   }
   if ($c++ != 0) print ",\n";
   print "   $count";

}
print "\n};\n";



$alarms = $this->helper->multicore->getLocalList("/OSEK", "ALARM");
$alarmstotal = count($alarms) + ($dyncount > 0 ? ($dyncount - 1) * count($dynalarms) : 0);
print "/** TODO replace next line with: \n";
print " ** AlarmVarType AlarmsVar[" . $alarmstotal . "]; */\n";
print "AlarmVarType AlarmsVar[" . $alarmstotal . "];\n\n";

print "const AlarmConstType AlarmsConst[" . $alarmstotal . "]  = {\n";

foreach ($alarms as $count=>$alarm)
{
   $alarmit = 1;
   $alarmdyn = 0;
   if(in_array($alarm, $dynalarms))
   {
      $alarmit = $dyncount;
      $alarmdyn = 1;
   }
   for($i = 0; $i < $alarmit ; $i++)
   {
      $alarmcounter = $this->config->getValue("/OSEK/" . $alarm, "COUNTER");
      $alarmaction = $this->config->getValue("/OSEK/" . $alarm, "ACTION");
      print "   {\n";
      print "      OSEK_COUNTER_" . $alarmcounter . ", /* Counter */\n";
      print "      " . $alarmaction . ", /* Alarm action */\n";
      print "      {\n";
      switch ($alarmaction)
      {
        case "INCREMENT":
          print "         NULL, /* no callback */\n";
          print "         0, /* no task id */\n";
          print "         0, /* no event */\n";
          print "         OSEK_COUNTER_" . $this->config->getValue("/OSEK/" . $alarm . "/INCREMENT","COUNTER") . " /* counter */\n";
          break;
        case "ACTIVATETASK":
          if ($alarmdyn == 1)
          {
            $task = "TASKS_COUNT + $i";
          }
          else
          {
            $task = $this->config->getValue("/OSEK/" . $alarm . "/ACTIVATETASK", "TASK");
          }
          print "         NULL, /* no callback */\n";
          print "         " . $task . ", /* TaskID */\n";
          print "         0, /* no event */\n";
          print "         0 /* no counter */\n";
          break;
        case "SETEVENT":
          if ($alarmdyn == 1)
          {
            $task = "TASKS_COUNT + $i";
          }
          else
          {
            $task = $this->config->getValue("/OSEK/" . $alarm . "/SETEVENT", "TASK");
          }
          print "         NULL, /* no callback */\n";
          print "         " . $task . ", /* TaskID */\n";
          print "         " . $this->config->getValue("/OSEK/" . $alarm . "/SETEVENT","EVENT") . ", /* event */\n";
          print "         0 /* no counter */\n";
          break;
        case "ALARMCALLBACK":
          print "         OSEK_CALLBACK_" . $this->config->getValue("/OSEK/" . $alarm . "/ALARMCALLBACK", "ALARMCALLBACKNAME") . ", /* callback */\n";
          print "         0, /* no taskid */\n";
          print "         0, /* no event */\n";
          print "         0 /* no counter */\n";
          break;
        default:
          $this->log->error("Alarm $alarm has an invalid action: $alarmaction");
          break;
      }
      print "      }\n";
      print "   },\n";
  }
}

print "\n};\n\n";

print "const AutoStartAlarmType AutoStartAlarm[ALARM_AUTOSTART_COUNT] = {\n";
$first = true;

foreach ($alarms as $count=>$alarm)
{
   if ($this->config->getValue("/OSEK/" . $alarm, "AUTOSTART") == "TRUE")
   {
      if ($first == false)
      {
         print ",\n";
      } else {
         $first = false;
      }
      print "  {\n";

      print "      " . $this->config->getValue("/OSEK/" . $alarm, "APPMODE") . ", /* Application Mode */\n";
      // print "      OSEK_COUNTER_" . $this->config->getValue("/OSEK/" . $alarm, "COUNTER") . ", /* Counter */\n";
      print "      $alarm, /* Alarms */\n";
      print "      " . $this->config->getValue("/OSEK/" . $alarm, "ALARMTIME") . ", /* Alarm Time */\n";
      print "      " . $this->config->getValue("/OSEK/" . $alarm, "CYCLETIME") . " /* Alarm Time */\n";
      print "   }";
   }
}
print "\n};\n\n";

$counters = $this->helper->multicore->getLocalList("/OSEK", "COUNTER");

print "CounterVarType CountersVar[" . count($counters) . "];\n\n";

$alarms = $this->config->getList("/OSEK","ALARM");

print "const CounterConstType CountersConst[" . count($counters) . "] = {\n";
foreach ($counters as $count=>$counter)
{
   if ($count!=0)
   {
      print ",\n";
   }
   print "   {\n";
   $countalarms = 0;
   foreach ($alarms as $alarm)
   {
    if ($dyncount > 0 && in_array($alarm, $dynalarms) && 
        $counter == $this->config->getValue("/OSEK/" . $alarm,"COUNTER"))
    {
      $countalarms += $dyncount;
    }
    else if ($counter == $this->config->getValue("/OSEK/" . $alarm,"COUNTER"))
    {
      $countalarms++;
    }
   }
   print "      $countalarms, /* quantity of alarms for this counter */\n";
   print "      (AlarmType*)OSEK_ALARMLIST_" . $counter . ", /* alarms list */\n";
   print "      " . $this->config->getValue("/OSEK/" . $counter,"MAXALLOWEDVALUE") . ", /* max allowed value */\n";
   print "      " . $this->config->getValue("/OSEK/" . $counter,"MINCYCLE") . ", /* min cycle */\n";
   print "      " . $this->config->getValue("/OSEK/" . $counter,"TICKSPERBASE") . " /* ticks per base */\n";
   print "   }";
}
print "\n};\n\n";

?>

/** TODO replace the next line with
 ** uint8 ApplicationMode; */
uint8 ApplicationMode;

/** TODO replace the next line with
 ** uint8 ErrorHookRunning; */
/* This variable needs to be initialized to 0. This is the normal
 * behaviour in C, but in embedded C sometimes to save startup time
 * variables which do not have initialization are not initialized.
 */
uint8 ErrorHookRunning = 0;

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/
<?php
$intnames = $this->helper->multicore->getLocalList("/OSEK", "ISR");

foreach ($intnames as $int)
{
   $inttype = $this->config->getValue("/OSEK/" . $int,"INTERRUPT");
   $intcat = $this->config->getValue("/OSEK/" . $int,"CATEGORY");

   if ($intcat == 2)
   {?>

void OSEK_ISR2_<?php print $int;?>(void)
{
   /* store the calling context in a variable */
   ContextType actualContext = GetCallingContext();
   /* set isr 2 context */
   SetActualContext(CONTEXT_ISR2);

   /* trigger isr 2 */
   OSEK_ISR_<?php print $int;?>();

   /* reset context */
   SetActualContext(actualContext);

#if (NON_PREEMPTIVE == OSEK_DISABLE)
   /* check if the actual task is preemptive */
   if ( ( CONTEXT_TASK == actualContext ) &&
        ( TasksConst[GetRunningTask()].ConstFlags.Preemtive ) )
   {
      /* this shall force a call to the scheduler */
      PostIsr2_Arch(isr);
   }
#endif /* #if (NON_PREEMPTIVE == OSEK_ENABLE) */
}

<?php }

}
?>

/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/