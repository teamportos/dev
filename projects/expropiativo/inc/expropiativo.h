/* Copyright 2017, Esteban Volentini - Facet UNT, Fi UNER
 * Copyright 2014, 2015 Mariano Cerdeiro
 * Copyright 2014, Pablo Ridolfi
 * Copyright 2014, Juan Cecconi
 * Copyright 2014, Gustavo Muro
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef EXPROPIATIVO_H    /*! @cond    */
#define EXPROPIATIVO_H    /*! @endcond */

/** @file expropiativo.h
 **
 ** @brief Ejemplo de un cambio de contexto expropiativo
 **
 ** Ejemplo de la implementación básica de la ejecución de dos tareas
 ** con un planitifador tipo round robin utilizando un cambio de contexto
 ** expropiativo basado en el temporizador del sistema para asignar las cuotas
 ** de tiempo de cada proceso.
 ** 
 ** | RV | YYYY.MM.DD | Autor       | Descripción de los cambios              |
 ** |----|------------|-------------|-----------------------------------------|
 ** |  3 | 2017.10.16 | evolentini  | Correción en el formato del archivo     |
 ** |  2 | 2017.09.21 | evolentini  | Cambio para utilizar drivers_bm de UNER |
 ** |  1 | 2016.06.25 | evolentini  | Version inicial del archivo             |
 ** 
 ** @defgroup ejemplos Proyectos de ejemplo
 ** @brief Proyectos de ejemplo de la Especialización en Sistemas Embebidos
 ** @{ 
 */

/* === Inclusiones de archivos externos ==================================== */

/* === Cabecera C++ ======================================================== */
#ifdef __cplusplus
extern "C" {
#endif

/* === Definicion y Macros ================================================= */

/* == Declaraciones de tipos de datos ====================================== */

/* === Declaraciones de variables externas ================================= */

/* === Declaraciones de funciones externas ================================= */

/** @brief Punto de entrada del programa
 **
 ** ResetISR es el punto de entrada al pre main del lenguaje c
 **
 ** @remark La definicion del simbolo esta en el archivo:
 **         externals/drivers/cortexM4/lpc43xx/src/cr_startup_lpc43xx.c
 **/
extern void ResetISR(void);

/** @brief Valor inical del puntero de pila
 **
 ** Declaración externa para el script del enlazador con el valor inicial
 ** del puntero de pila.
 **
 ** @remark Solo se necesita la declaración, la dafinicion no es necesaria
 **         ya que se define el valor en el script del enlazador que está en
 **         el archivo:
 **         externals/base/cortexM4/lpc43xx/linker/ciaa_lpc4337.ld.
 **/
extern void _vStackTop(void);

/** @brief Funcion que implementa el cambio de contexto
 **
 ** Cada vez que se llama a la función la misma almacena el contexto de la 
 ** tarea que la llama, selecciona a la otra tarea como activa y recupera
 ** el contexto de la misma.
 **
 ** @remark Esta función se ejecuta cada vez que expira el temporizador del
 **         sistema, por lo cual se agrega como SysTick Handler en el vector
 **         de interrupciones.
 */

 void CambioContexto(void);

/* === Ciere de documentacion ============================================== */
#ifdef __cplusplus
}
#endif

/** @} Final de la definición del modulo para doxygen */

#endif   /* EXPROPIATIVO_H */
