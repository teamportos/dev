/* Copyright 2017, XXXX
 *
 *  * All rights reserved.
 *
 * This file is part of CIAA Firmware.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** \brief Blinking Bare Metal example source file
 **
 ** This is a mini example of the CIAA Firmware.
 **
 **/

/** \addtogroup CIAA_Firmware CIAA Firmware
 ** @{ */

/** \addtogroup Examples CIAA Firmware Examples
 ** @{ */
/** \addtogroup Baremetal Bare Metal example source file
 ** @{ */

/*
 * Initials     Name
 * ---------------------------
 *
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * yyyymmdd v0.0.1 initials initial version
 */

/*==================[inclusions]=============================================*/
#include "serial.h"       /* <= own header */
#include "task.h"
#include "event_groups.h"
#include "stdint.h"
#include "led.h"
#include "switch.h"
#include "uart.h"
#include <string.h>

/*==================[macros and definitions]=================================*/

#define EVENTO_INICIAR 0x01
#define EVENTO_COMPLETO 0x02

typedef struct {
   const char * datos;
   uint8_t cantidad;
   uint8_t enviados;
} cola_t;

/*==================[internal data declaration]==============================*/

cola_t cola;

EventGroupHandle_t eventos;

/*==================[internal functions declaration]=========================*/

bool EnviarCaracter(void);

bool EnviarTexto(const char * cadena);

void Blinking(void * parametros);

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

bool EnviarCaracter(void){
   uint8_t eventos;
   bool completo = FALSE;

   eventos = Chip_UART_ReadLineStatus(USB_UART);

   if (eventos & UART_LSR_THRE) {
      Chip_UART_SendByte(USB_UART, cola.datos[cola.enviados]);
      cola.enviados++;

      if (cola.enviados == cola.cantidad) {
         Chip_UART_IntDisable(USB_UART, UART_IER_THREINT);
         completo = TRUE;
      }
   }
   return (completo);
}

bool EnviarTexto(const char * cadena) {
   bool pendiente = FALSE;

   cola.datos = cadena;
   cola.cantidad = strlen(cadena);
   cola.enviados = 0;

   if (cola.cantidad) {
      Chip_UART_SendByte(USB_UART, cola.datos[cola.enviados]);
      cola.enviados++;

      if (cola.enviados < cola.cantidad) {
         Chip_UART_IntEnable(USB_UART, UART_IER_THREINT);
         pendiente = TRUE;
      }
   }
   return (pendiente);
}
/*---------------------------------------------------------------------------*/
void Teclado(void * parametros) {
   uint8_t tecla;
   uint8_t anterior = 0;

   while(1) {
      tecla = Read_Switches();
      if (tecla != anterior) {
         switch(tecla) {
            case TEC1:
                  xEventGroupSetBits(eventos, EVENTO_INICIAR);
               break;
            case TEC2:
               break;
            case TEC3:
               break;
            case TEC4:
               break;
         }
         anterior = tecla;
      }
      vTaskDelay(100);
      Led_Toggle(GREEN_LED);
   }
}

void Transmitir(void * parametros) {
   static const char cadena[] = "Hola Mundo\r\n";
   
   while(1) {

      while(xEventGroupWaitBits(eventos, EVENTO_INICIAR, 
         TRUE, FALSE, portMAX_DELAY) == 0);

      Led_On(YELLOW_LED);
      if (EnviarTexto(cadena)) {
         while(xEventGroupWaitBits(eventos, EVENTO_COMPLETO, 
            TRUE, FALSE, portMAX_DELAY) == 0);
      }
      Led_Off(YELLOW_LED);
   }
}
/*==================[external functions definition]==========================*/

void EventoSerial(void) {
   if (EnviarCaracter()) {
      xEventGroupSetBits(eventos, EVENTO_COMPLETO);
   };
}

/** \brief Main function
 *
 * This is the main entry point of the software.
 *
 * \returns 0
 *
 * \remarks This function never returns. Return value is only to avoid compiler
 *          warnings or errors.
 */

int main(void) {

   /* inicializaciones */
   Init_Leds();
   Init_Switches();
   Init_Uart_Ftdi();

   NVIC_EnableIRQ(26);

   eventos = xEventGroupCreate();
   if (eventos != NULL) {
      xTaskCreate(Teclado, "Teclado", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY + 2, NULL);
      xTaskCreate(Transmitir, "Serial", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY + 1, NULL);
      vTaskStartScheduler();
   }

   for(;;);
	return 0;
}

/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/

