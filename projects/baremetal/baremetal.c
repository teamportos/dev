#include "baremetal.h"
#include "stdint.h"
#include "led.h"
#include "switch.h"
#include "chip.h"

#define CONST_TIMER 3000

void Delay (uint8_t c){
uint64_t i;
for (i=CONST_TIMER * c;i!=0;i--);
}

int main(void)
{
  	uint8_t led_prendido[]={LED_B,LED_1,LED_2,LED_3}, tecla, tec1 = 0, i = 0;
	InitLeds();
	Init_Switches();
	uint64_t iter = 0;
	ToggleLed(led_prendido);
	while(1)
	{
		tecla = Read_Switches();
		if (tecla == TEC1 && tec1 != TEC1){
			ApagarLed(led_prendido[i]);
			i = (i+1) % 4;
		}
		tec1 = tecla;
		if (iter % CONST_TIMER == 0) ToggleLed(led_prendido[i]);
		iter++;
	}
   return 0;
}
