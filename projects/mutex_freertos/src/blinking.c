/* Copyright 2017, XXXX
 *
 *  * All rights reserved.
 *
 * This file is part of CIAA Firmware.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** \brief Blinking Bare Metal example source file
 **
 ** This is a mini example of the CIAA Firmware.
 **
 **/

/** \addtogroup CIAA_Firmware CIAA Firmware
 ** @{ */

/** \addtogroup Examples CIAA Firmware Examples
 ** @{ */
/** \addtogroup Baremetal Bare Metal example source file
 ** @{ */

/*
 * Initials     Name
 * ---------------------------
 *
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * yyyymmdd v0.0.1 initials initial version
 */

/*==================[inclusions]=============================================*/
#include "blinking.h"       /* <= own header */
#include "task.h"
#include "semphr.h"
/*==================[macros and definitions]=================================*/
typedef struct {
   uint8_t led;
   uint16_t delay;
   SemaphoreHandle_t mutex;
} blinking_t;

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

void Blinking(void * parametros) {
   blinking_t * valores = parametros;

   while(1) {
      if (valores->mutex != NULL) xSemaphoreTake(valores->mutex, portMAX_DELAY);
      Led_On(valores->led);
      vTaskDelay(valores->delay);
      Led_Off(valores->led);
      if (valores->mutex != NULL) xSemaphoreGive(valores->mutex);
      vTaskDelay(valores->delay);
   }
}

int main(void)
{
   SemaphoreHandle_t mutex;
   static blinking_t valores[] = {
      { .led = RGB_R_LED, .delay = 700 , .mutex = NULL },
      { .led = RGB_B_LED, .delay = 900 , .mutex = NULL },
      { .led = YELLOW_LED, .delay = 250, .mutex = NULL },
   };

   /* inicializaciones */
	Init_Leds();

   /*acá va mi programa principal */
   mutex = xSemaphoreCreateMutex();
   if (mutex != NULL) {
      valores[0].mutex = mutex;
      valores[1].mutex = mutex;
      xTaskCreate(Blinking, "Rojo", configMINIMAL_STACK_SIZE, (void *) &valores[0], 1, NULL);
      xTaskCreate(Blinking, "Azul", configMINIMAL_STACK_SIZE, (void *) &valores[1], 2, NULL);
      xTaskCreate(Blinking, "Amarilla", configMINIMAL_STACK_SIZE, (void *) &valores[2], 1, NULL);
      vTaskStartScheduler();
   }

   for(;;);

	return 0;
}

/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/

