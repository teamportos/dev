/* Copyright 2017, XXXX
 *
 *  * All rights reserved.
 *
 * This file is part of CIAA Firmware.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** \brief Blinking Bare Metal example source file
 **
 ** This is a mini example of the CIAA Firmware.
 **
 **/

/** \addtogroup CIAA_Firmware CIAA Firmware
 ** @{ */

/** \addtogroup Examples CIAA Firmware Examples
 ** @{ */
/** \addtogroup Baremetal Bare Metal example source file
 ** @{ */

/*
 * Initials     Name
 * ---------------------------
 *
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * yyyymmdd v0.0.1 initials initial version
 */

/*==================[inclusions]=============================================*/
#include "eventos.h"       /* <= own header */
#include "task.h"
#include "event_groups.h"

/*==================[macros and definitions]=================================*/
#define EVENTO_TECLA 0x01

typedef struct {
   uint8_t led;
   uint16_t delay;
   EventGroupHandle_t evento;
} blinking_t;

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/
/** \brief Main function
 *
 * This is the main entry point of the software.
 *
 * \returns 0
 *
 * \remarks This function never returns. Return value is only to avoid compiler
 *          warnings or errors.
 */
void Blinking(void * parametros) {
   blinking_t * valores = parametros;
   while(1) {
      while(xEventGroupWaitBits(valores->evento, EVENTO_TECLA, TRUE, FALSE, valores->delay) == 0);
      Led_On(valores->led);
      resultado = xEventGroupWaitBits(valores->evento, EVENTO_TECLA, TRUE, FALSE, valores->delay);
      Led_Off(valores->led);
   }
}

void Teclado(void * parametros) {
   uint8_t tecla;
   uint8_t anterior = 0;
   EventGroupHandle_t evento = parametros;

   while(1) {
      tecla = Read_Switches();
      if (tecla != anterior) {
         switch(tecla) {
            case TEC1:
                  xEventGroupSetBits(evento, EVENTO_TECLA);
               break;
         }
         anterior = tecla;
      }
      vTaskDelay(250);
      Led_Toggle(GREEN_LED);
   }
}

int main(void)
{
   EventGroupHandle_t evento;
   static blinking_t valores = { .led = RED_LED, .delay = 3000 };

   /* inicializaciones */
	Init_Leds();
   Init_Switches();
   evento = xEventGroupCreate();
   if (evento != NULL) {
      /*acá va mi programa principal */
      valores.evento = evento;
      xTaskCreate(Teclado, "Teclado", configMINIMAL_STACK_SIZE, (void *) evento, 1, NULL);
      xTaskCreate(Blinking, "Azul", configMINIMAL_STACK_SIZE, (void *) &valores, 2, NULL);
      vTaskStartScheduler();
   }
   
   for(;;);

	return 0;
}

/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/

