/* Copyright 2018, Miguel Liezun <liezun.js@gmail.com>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/** @file teclado.c
 **
 ** @brief Ejemplo de teclado siendo leido por multiples tareas dinamicas
 **
 ** Ejemplo donde cada tecla es leída por una tarea independiente que se genera
 ** a partir de una única función que abstrae el comportamiento genérico.
 ** 
 ** | RV | YYYY.MM.DD | Autor       | Descripción de los cambios              |
 ** |----|------------|-------------|-----------------------------------------|
 ** |  1 | 2018.08.30 | mlieuzn     | Version inicial del archivo             |
 ** 
 ** @defgroup ejemplos Proyectos de ejemplo
 ** @brief Proyectos de ejemplo
 ** @{ 
 */

/* === Inclusiones de cabeceras ============================================ */
#include <stdint.h>
#include "teclado.h"
#include "led.h"
#include "switch.h"
#include "os.h"


/* === Definicion y Macros ================================================= */

/* === Declaraciones de tipos de datos internos ============================ */

struct TeclaLED {
   uint8_t Tecla;
   uint8_t LED;
};

/* === Declaraciones de funciones internas ================================= */

/* === Definiciones de variables internas ================================== */

struct TeclaLED tl[] = {
  {TEC1, RGB_B_LED},
  {TEC2, RED_LED},
  {TEC3, YELLOW_LED},
  {TEC4, GREEN_LED}
};

/* === Definiciones de variables externas ================================== */

/* === Definiciones de funciones internas ================================== */

void LeerTecla() {
   struct TeclaLED* tl = GetDynamicParam();
   if (tl != NULL && Read_Switches() == tl->Tecla)
   {
      Led_Toggle(tl->LED);
   }
   TerminateTask();
}

/** @brief Tarea de configuración
 **
 ** Esta tarea arranca automaticamente en el modo de aplicacion Normal.
 */
TASK(Configuracion) {

   /* Inicializaciones y configuraciones de dispositivos */
   Init_Leds();
   Init_Switches();

   TaskType tareas[] = {
      NewTask(LeerTecla, &tl[0]),
      NewTask(LeerTecla, &tl[1]),
      NewTask(LeerTecla, &tl[2]),
      NewTask(LeerTecla, &tl[3])
   };

   SetRelAlarm(GetDynamicAlarm(TareasDinamicas, tareas[0]), 500, 500);
   SetRelAlarm(GetDynamicAlarm(TareasDinamicas, tareas[1]), 500, 500);
   SetRelAlarm(GetDynamicAlarm(TareasDinamicas, tareas[2]), 500, 500);
   SetRelAlarm(GetDynamicAlarm(TareasDinamicas, tareas[3]), 500, 500);

   /* Terminación de la tarea */
   TerminateTask();
}

/* === Definiciones de funciones externas ================================== */

/** @brief Función para interceptar errores
 **
 ** Esta función es llamada desde el sistema operativo si una función de 
 ** interface (API) devuelve un error. Esta definida para facilitar la
 ** depuración y detiene el sistema operativo llamando a la función
 ** ShutdownOs.
 **
 ** Los valores:
 **    OSErrorGetServiceId
 **    OSErrorGetParam1
 **    OSErrorGetParam2
 **    OSErrorGetParam3
 **    OSErrorGetRet
 ** brindan acceso a la interface que produjo el error, los parametros de 
 ** entrada y el valor de retorno de la misma.
 **
 ** Para mas detalles consultar la especificación de OSEK:
 ** http://portal.osek-vdx.org/files/pdf/specs/os223.pdf
 */
void ErrorHook(void) {
   Led_On(RED_LED);
   ShutdownOS(0);
}

/** @brief Función principal del programa
 **
 ** @returns 0 La función nunca debería terminar
 **
 ** \remark En un sistema embebido la función main() nunca debe terminar.
 **         El valor de retorno 0 es para evitar un error en el compilador.
 */
int main(void) {

   /* Inicio del sistema operatvio en el modo de aplicación Normal */
   StartOS(Normal);

   /* StartOs solo retorna si se detiene el sistema operativo */
   while(1);

   /* El valor de retorno es solo para evitar errores en el compilador*/
   return 0;
}

/* === Ciere de documentacion ============================================== */

/** @} Final de la definición del modulo para doxygen */

