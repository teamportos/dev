# Portos - Requirements Specifications

**1    Introduction **

**1.1    Purpose**

**#**DONE

The following paper has the purpose of defining the functional and non functional specifications for the development of a Real Time Operating System for Embedded Systems, which will be destined to developers and will allow an agile and flexible workflow. 

\

**1.2    System Scope**

\#DONE

This requirements specification is destined to the OS designers, in order to establish the general lines to its development. 

The principal objective of this project is to guarantee a trustworthy system for the development of applications and tasks in embedded systems, taking the best concepts from existing RTOS and improving them. The proposed name to this Operating System is Portos (**Po**rtable **R**eal **T**ime **O**perating **S**ystem), and it will ease the management of resources and tasks with different priority levels and provide tools for concurrency and synchronization, but as it will be only constituted by a kernel, the management of devices will be in the hands of the developer or an upper framework.

\[se deberia especificar el porte de sistema embebido al que se esta apuntando\] 

\
\

**1.3    Definitions, abbreviations and acronyms**

\#DONE

* OS: **O**perating **S**ystem
* RTOS: **R**eal **T**ime **O**perating **S**ystem
* Portos: **Po**rtable **R**eal **T**ime **O**perating **S**ystem

\

**1.4    References**

\# TODO

**1.5    General Vision of the Document**

\# DONE

The following document will contain complete information about Portos, and it will be divided in the sections that are descripted below. The topics that we will include are descriptions, functions, characteristics, restrictions, dependencies and assumptions, requirements, system attributes. To explain each of the aforementioned, we will divide the document into sections, following the IEEE 830 standard, that will be named according the topic to be developed.

\

**2.    General Description**

\

**2.1.     Product Perspective**

\# DONE

This operating system is intended to be an extended implementation of OSEK, this mean that this OS will be compatible with any project made for an OSEK platform without much effort, and will extend OSEK capabilities adding system calls for dynamic task depletion. Portos will maintain the OSEK clean architecture, and the added functions will be simple and concise.

\[agregar independencia de plataforma\]

**2.2.     Product Functions**

\# DONE

Portos is intended to be a Real Time kernel based on OSEK, which will allow the users to create their own applications, ensuring an optimum management of processor time and resources, and providing indispensable services to the applications. As it is a platform independent kernel, it will not include functions to manage devices.

**2.3.     Characteristics of Users **

\# DONE

The intended users for this projects are developers of Embedded Systems.

As said before, the project will be a platform independent kernel, so the implementation of certain parts of Portos will be in hands of the final user.

The idea is to create a community of users and companies that share their platform specific implementations, so they can make easier for others to start develop a new project.

**2.4.     Restrictions**

\# REVIEW

Restrictions for the design of the OS:

* Platform independence
* Lightweight
* OSEK back compatible
* Dynamic task deployment
* Simple and concise system calls

**2.5.     Dependencies and assumptions **

\# REVIEW

Given that the project is platform independent, it does not have any dependency.

**2.6.     Future requirements **

\# DONE

As a future improvement for the product, higher-level services must be created, in order to give Portos the capacity to evolve from a Real Time Kernel to a Real Time Operating System. Some services that Portos will need are: 

* A framework for developing graphical user interfaces. 
* An Integrated Development Enviroment (IDE). 
* Drivers to manage I/O devices.
* File management.
* Support for communication protocols.
* Hardware abstraction layer.

\

**3.     Specific requirements**

\

**3.1.     External interfaces **

\# REVIEW

Portos will expose functions for integration with other projects like: TCP/IP stack implementation or LoRaWAN implementation, etcetera.

\[puede no ir\]

**3.2.     Functions (o servicios)**

\# TODO

**3.3.     Performance requirements**

\# TODO

**3.4.     Design restrictions**

\# TODO

**3.5.     System attributes**

\#PARTIAL (Pablo)

Compatibility: It was used as a basis of the osek project our operating system will be compatible with the devices that implement osek implemented a minimum of changes.

Lightweight: A fully functional kernel was developed taking care to make excessive use of the device's memory. 

**3.6.     Other requirements**

\# TODO
